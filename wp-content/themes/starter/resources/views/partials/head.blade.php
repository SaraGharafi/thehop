<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">

  <link rel="icon" type="image/png" href="@asset('images/favicon.png')" sizes="16x16">

  @php wp_head() @endphp
</head>
