<div class="c-hero">

    <div class="c-hero__container">
      <div class="c-hero__container__text">
        <span class="c-hero__pretitle">premios thehop</span>
        <h1 class="c-hero__title">
          Es hora de dar un salto <span class="c-hero__plus">+</span> allá
        </h1>

        <div class="c-hero__description">
          <p>
            <strong>TheHop, el Programa de Emprendimiento Colaborativo de Estrella Galicia</strong> sigue evolucionando para conectar el mejor talento externo con nuestra marca. Si tienes una startup tecnológico-digital, éste es nuestro momento. <strong>Saltemos juntos. Saltemos + lejos.</strong>
          </p>
        </div>

        <a href="http://pre7.pixeldivision.es/design-system/documentacion/framework/" class="c-hero__cta o-button o-button--color-transparent" target="_blank">
          Descubre el programa
          <i class="u-icon--arrow-right "></i>
        </a>

        <div class="c-hero__pretitle--final">Empieza ahora</div>
      </div>

      <video class="c-hero__container__video" autoplay muted="muted" loop>
        <span class="c-hero__play"></span>
        <source src="https://thehop.xyz/wp-content/uploads/2019/05/THEHOP_VIDEOLOOP.mp4" type="video/mp4">
      </video>
    </div>

</div>
