<footer class="c-footer">
  <div class="u-wrapper">
    <div class="c-footer__container">
      <a class="c-footer__logo" href="#"><img src="@asset('images/logo.svg')" alt="Pixel Division"></a>

      <nav class="c-footer__menu c-footer__menu--main">
        <a href="#">Sobre nosotros</a>
        <a href="#">Nuestros trabajos</a>
        <a href="#">Servicios</a>
        <a href="#">Trabaja con nosotros</a>
        <a href="#">Contacto</a>
      </nav>
    </div>

    <div class="c-footer__container c-footer__container--bottom">
      <span>© Pixel Division 2019.</span>

      <nav class="c-footer__menu">
        <a href="#">Política de privacidad</a>
        <a href="#">Política de cookies</a>
      </nav>
    </div>
  </div>
</footer>

<script src="https://cdn.jsdelivr.net/npm/css-vars-ponyfill@2"></script>
<script>
  cssVars();
</script>



