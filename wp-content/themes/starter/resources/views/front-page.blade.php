@extends('layouts.app')

@section('content')
  @include('partials.components.hero')
  @include('partials.page-home.content')
@endsection
