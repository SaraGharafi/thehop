<?php include(__DIR__ . '/../../template-parts/head.php'); ?>
<?php
$scan = scandir($path_modules);
?>
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="">
            <div class="u-uikit__header">
                <p class="u-uikit__header__pretitle">Modules</p>
                <p class="u-uikit__header__title">Web modules</p>
            </div>

            <?php
            if(count($scan)!=2){
                $modules_directory = array_diff($scan, array('..', '.'));
                foreach ($modules_directory as $key => $module): ?>
                    <div class="card">
                        <div class="card-body u-uikit__card-body">
                            <div>
                                <div class="u-uikit__card-body__title"><?= str_replace('.blade.php','',$module); ?></div>
                                <div class="u-uikit__card-body__content u-uikit__card-body__content--full">
                                    <div class="u-uikit__card-body__content-wrapper">
                                        <div class="u-uikit__card-body__content-info-code">
                                            <p class="u-uikit__card-body__content-title">Module</p>
                                            <p class="u-uikit__card-body__content-copy"><code class="highlighter-rouge"><?= $module; ?></code></p>
                                        </div>
                                    </div>
                                    <div class="u-uikit__card-body__content-text">
                                        <?php
                                        echo file_get_contents($path_modules.'/'.$module);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach;
            }else{ ?>
                <div class="card">
                    <div class="card-body u-uikit__card-body">
                        <div>
                            <div class="u-uikit__card-body__content">
                                <div class="u-uikit__card-body__content-text">
                                    <p>Para ver el listado de modulos tienes que crearlos dentro del proyecto en la carpeta uikit dentro de views</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
    </div>
</div>
<?php include(__DIR__ . '/../../template-parts/footer.php'); ?>
