<div class="card-header u-uikit__card_title">Fonts</div>

<?php
    foreach ($fonts['theme_fonts'] as $font_key => $font): ?>
      <div class="card u-uikit__fonts__block-content u-uikit__block">
        <div class="card-body">
          <div class="u-uikit__fonts__block">
            <div class="style-font-type">Font <?= $font['type']; ?></div>
            <div class="style-font-name-big" style="font-family:var(--font-family-<?= $font['type']; ?>);font-weight:var(--font-weight-<?= $font['weight']; ?>)"><?= $font['name']; ?></div>
            <div class="style-font-font-type"><?= $font['name']; ?> <?= $font['weight']; ?></div>
            <div class="style-font-text" style="font-family:var(--font-family-<?= $font['type']; ?>);font-weight:var(--font-weight-<?= $font['weight']; ?>)">ABCDEFGHIJKLMNOPQRSTUVWXYZ</div>
            <div class="style-font-text" style="font-family:var(--font-family-<?= $font['type']; ?>);font-weight:var(--font-weight-<?= $font['weight']; ?>)">abcdefghijklmnopqrstuvwxyz</div>
            <div class="style-font-text" style="font-family:var(--font-family-<?= $font['type']; ?>);font-weight:var(--font-weight-<?= $font['weight']; ?>)">0123456789.,@#$%^&*()[]</div>
          </div>
        </div>
      </div>
    <?php endforeach; ?>

<div class="card">
  <div class="card-header u-uikit__card_title">Font Usage</div>
  <div class="card-body">
      <?php
      foreach ($fonts['font_usage'] as $font_usage_key => $font_usage): ?>
        <div class="u-uikit__fonts__block">
          <div class="style-font-text" style="font-family:var(--font-family-<?= $font_usage['type']; ?>);font-weight:var(--font-weight-<?= $font_usage['weight']; ?>)"><?= $example_texts[array_rand($example_texts)]; ?></div>
          <div class="style-font-name"><?= $font_usage['name']; ?> (<?= $font_usage['weight']; ?>)</div>
        </div>
      <?php endforeach; ?>
  </div>
</div>
