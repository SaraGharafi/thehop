<div class="card-header u-uikit__card__title">Headings</div>
<?php
$headings = array(
    array('heading' => 'h1', 'code' => '<h1></h1>'),
    array('heading' => 'h2', 'code' => '<h2></h2>'),
    array('heading' => 'h3', 'code' => '<h3></h3>'),
    array('heading' => 'h4', 'code' => '<h4></h4>'),
    array('heading' => 'h5', 'code' => '<h5></h5>'),
    array('heading' => 'h6', 'code' => '<h6></h6>'),
); ?>

<?php
foreach ($headings as $heading_key => $heading):
    $jskey = $heading['heading'].'-'.$heading_key; ?>
<div class="card">
    <div class="card-body u-uikit__card-body">
      <div>
        <div class="u-uikit__card-body__title"><?= $heading['heading'] ?></div>
        <div class="u-uikit__card-body__content">
          <div class="u-uikit__card-body__content-wrapper">
            <div class="u-uikit__card-body__content-info">
              <div class="u-uikit__card-body__content-block">
                <p class="u-uikit__card-body__content-title">Family</p>
                <p class="u-uikit__card-body__content-copy js-font-family-<?= $jskey ?>"></p>
              </div>
              <div class="u-uikit__card-body__content-block">
                <p class="u-uikit__card-body__content-title">Size</p>
                <p class="u-uikit__card-body__content-copy js-font-size-<?= $jskey ?>"></p>
              </div>
              <div class="u-uikit__card-body__content-block">
                <p class=" u-uikit__card-body__content-title">Line Height</p>
                <p class="u-uikit__card-body__content-copy js-font-line-height-<?= $jskey ?>"></p>
              </div>
            </div>
            <div class="u-uikit__card-body__content-info-code">
              <p class="u-uikit__card-body__content-title">Code</p>
              <p class="u-uikit__card-body__content-copy"><code class="highlighter-rouge"><?= htmlentities($heading['code']) ?></code></p>
            </div>

          </div>
          <div class="u-uikit__card-body__content-text u-uikit__card-body__content-text--font">
            <<?= $heading['heading'] ?> class="js-theme-font" data-font="<?= $jskey ?>"><?= $example_texts[array_rand($example_texts)];?></<?= $heading['heading'] ?>>
        </div>
      </div>
    </div>
  </div>
</div>
<?php endforeach; ?>
