<div class="card-header u-uikit__card__title">Form default</div>

<div class="card">
    <div class="card-body u-uikit__card-body">
        <div>
            <div class="u-uikit__card-body__content u-uikit__card-body__content--full">
                <div class="u-uikit__card-body__content-wrapper u-uikit__card-body__content-wrapper--full">
                    <div>
                        <p class="u-uikit__card-body__content-title">Code</p>
                        <p class="u-uikit__card-body__content-copy"><code class="highlighter-rouge">o-form</code></p>
                    </div>
                </div>
                <div class="u-uikit__card-body__content-text u-uikit__card-body__content-text--form">
                    <form class="o-form">
                        <div class="u-grid">
                            <div class="col-6 col-12@mobile-wide">
                                <label class="o-form__label" for="inputName">Name:</label>
                                <input class="o-form__control" type="text" name="inputName" id="inputName" required="">
                            </div>

                            <div class="col-6 col-12@mobile-wide">
                                <label class="o-form__label" for="inputEmail">Email:</label>
                                <input class="o-form__control" type="email" name="inputEmail" id="inputEmail" placeholder="email@myemail.com">
                            </div>

                            <div class="col-6 col-12@mobile-wide">
                                <label class="o-form__label" for="inputPhone">Teléfono:</label>
                                <input class="o-form__control" type="tel" name="inputPhone" id="inputPhone">
                            </div>

                            <div class="col-6 col-12@mobile-wide">
                                <label class="o-form__label" for="select">País:</label>
                                <select class="o-form__control" name="select" id="select">
                                    <option value="1">Alemania</option>
                                    <option value="1">España</option>
                                    <option value="1">Francia</option>
                                    <option value="1">Italia</option>
                                </select>
                            </div>

                            <div class="col-12">
                                <label class="o-form__label" for="textarea">Textarea:</label>
                                <textarea class="o-form__control" name="textarea" id="textarea"></textarea>
                            </div>

                            <div class="col-12 o-form__submit">
                                <input class="o-button o-button--color-primary" type="submit" value="Enviar">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
