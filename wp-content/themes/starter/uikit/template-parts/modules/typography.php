<?php
include(__DIR__ . '/../../template-parts/head.php');
$module = 'typography'; ?>

<div class="container-fluid u-uikit__fonts">
    <div class="animated fadeIn">
        <div class="u-wrapper u-wrapper--narrow">
            <div class="u-uikit__header">
                <p class="u-uikit__header__pretitle">Style</p>
                <p class="u-uikit__header__title">Typography</p>
            </div>
            <?php include('parts/typography/fonts.php'); ?>
            <?php include('parts/typography/headings.php'); ?>
            <?php include('parts/typography/headers.php'); ?>
            <?php include('parts/typography/paragraphs.php'); ?>
            <?php include('parts/typography/links.php'); ?>
            <?php include('parts/typography/lists.php'); ?>
        </div>
    </div>
</div>

<?php include(__DIR__ . '/../../template-parts/footer.php'); ?>
