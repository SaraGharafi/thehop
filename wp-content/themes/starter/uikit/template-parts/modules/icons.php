<?php include(__DIR__ . '/../../template-parts/head.php'); ?>

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="u-wrapper u-wrapper--narrow">
            <div class="u-uikit__header">
                <p class="u-uikit__header__pretitle">Style</p>
                <p class="u-uikit__header__title">Icons</p>
            </div>

            <div class="card-header u-uikit__card__title">Logos</div>
            <div class="card u-uikit__card--noborder">
                <div class="card-body u-uikit__card-body">
                    <div>
                        <div class="u-uikit__card-body__content">
                            <div class="u-uikit__card-body__content-wrapper u-uikit__card-body__content-wrapper--full">
                                <div class="u-uikit__logos">
                                    <?php
                                    foreach ($icons['logos'] as $key => $logo): ?>
                                        <div class="logo-block">
                                            <div class="logo-block-bg <?php if ($logo['type'] == 'dark'): ?>u-uikit--bg-grey<?php endif; ?>"><img src="<?= $logo['url']; ?>" alt="Logo"></div>
                                            <p class="logo-block-text"><?= $logo['name']; ?></p>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="card-header u-uikit__card__title">Icons</div>
            <div class="card-header u-uikit__card__title">Theme Icons</div>
            <div class="card u-uikit__card--noborder">
                <div class="card-body u-uikit__card-body">
                    <div class="u-uikit__card-body__content u-uikit__card-body__content--full">
                        <div class="u-uikit__card-body__content-info-code">
                            <p class="u-uikit__card-body__content-title">Code</p>
                            <p class="u-uikit__card-body__content-copy"><code class="highlighter-rouge">u-icon</code></p>
                        </div>
                        <div class="u-uikit__card-body__content-wrapper u-uikit__card-body__content-wrapper--full">
                            <div class="u-uikit__icons">
                                <?php
                                $icons = array();
                                $folder = $path_theme_icons;
                                $fn = fopen($folder, "r");
                                while (!feof($fn)) {
                                    $result = fgets($fn);
                                    if (strpos($result, '$u-icon') !== false && strpos($result, 'content') === false):
                                        $icon_array = explode(':', $result);
                                        $ico = str_replace('$u-icon-', '', $icon_array[0]);
                                        $icons[] = $ico;
                                    endif;
                                }
                                fclose($fn);
                                foreach ($icons as $icon_key => $icon): ?>
                                    <div class="icon-block">
                                        <div class="icon-block-bg">
                                            <i class="u-icon u-icon--<?= $icon ?>"></i>
                                        </div>
                                        <p class="icon-block-text"><?= $icon ?></p>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="u-uikit__card-body__content-code">
                        <pre><code class="language-markup"><?= htmlentities('<i class="u-icon--facebook u-color--facebook"></i>'); ?></code></pre>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<?php include(__DIR__ . '/../../template-parts/footer.php'); ?>
