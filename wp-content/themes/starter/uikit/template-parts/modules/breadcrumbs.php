<?php include(__DIR__ . '/../../template-parts/head.php'); ?>
<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="u-wrapper u-wrapper--narrow">
      <div class="u-uikit__header">
        <p class="u-uikit__header__pretitle">Navigation</p>
        <p class="u-uikit__header__title">Breadcrumbs</p>
      </div>

      <div class="card">
        <div class="card-body u-uikit__card-body">
          <div>
            <div class="u-uikit__card-body__title">Default</div>
            <div class="u-uikit__card-body__content">
              <div class="u-uikit__card-body__content-wrapper">
                <div>
                  <p class="u-uikit__card-body__content-title">Code</p>
                  <p class="u-uikit__card-body__content-copy"><code class="highlighter-rouge">c-breadcrumbs</code></p>
                </div>
              </div>
              <div class="u-uikit__card-body__content-text">

                  <!-- Aqui va el código -->

              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<?php include(__DIR__ . '/../../template-parts/footer.php'); ?>
