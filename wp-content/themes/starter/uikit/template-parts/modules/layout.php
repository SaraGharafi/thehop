<?php include(__DIR__ . '/../../template-parts/head.php'); ?>
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="u-wrapper u-wrapper--narrow">
            <div class="u-uikit__header">
                <p class="u-uikit__header__pretitle">Style</p>
                <p class="u-uikit__header__title">Layout</p>
            </div>

            <div class="card-header u-uikit__card__title">Grid system</div>
            <?php
            foreach ($layouts['grid'] as $key => $grid): ?>
                <div class="card u-uikit__card--noborder">
                    <div class="card-body u-uikit__card-body">
                        <div class="u-uikit__card-body__content-info">
                            <div class="u-uikit__card-body__content-block">
                                <p class="u-uikit__card-body__content-title"><?= $grid['name'] ?></p>
                                <p class="u-uikit__card-body__content-copy"><?= $grid['width'] ?></p>
                            </div>
                        </div>
                        <div class="u-uikit__grid-system">
                            <div class="u-uikit__grid-system__container u-uikit__grid-system__container--narrow<?= $key; ?>">
                                <div class="u-uikit__grid-system__block"></div>
                                <div class="u-uikit__grid-system__block"></div>
                                <div class="u-uikit__grid-system__block"></div>
                                <div class="u-uikit__grid-system__block"></div>
                                <div class="u-uikit__grid-system__block"></div>
                                <div class="u-uikit__grid-system__block"></div>
                                <div class="u-uikit__grid-system__block"></div>
                                <div class="u-uikit__grid-system__block"></div>
                                <div class="u-uikit__grid-system__block"></div>
                                <div class="u-uikit__grid-system__block"></div>
                                <div class="u-uikit__grid-system__block"></div>
                                <div class="u-uikit__grid-system__block"></div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>


            <div class="card-header u-uikit__card__title">Wrapper</div>
            <?php
            foreach ($layouts['wrappers'] as $key => $wrapper): ?>
            <div class="card u-uikit__card--noborder">
                <div class="card-body u-uikit__card-body">
                    <div>
                        <div class="u-uikit__card-body__content">
                            <div class="u-uikit__card-body__content-wrapper">
                                    <div class="u-uikit__card-body__content-info">
                                        <div class="u-uikit__card-body__content-block">
                                            <p class="u-uikit__card-body__content-title"><?= $wrapper['name'] ?></p>
                                            <p class="u-uikit__card-body__content-copy"><?= $wrapper['width'] ?></p>
                                        </div>
                                    </div>
                                    <div class="u-uikit__card-body__content-info-code">
                                        <p class="u-uikit__card-body__content-title">Code</p>
                                        <p class="u-uikit__card-body__content-copy"><code class="highlighter-rouge"><?= $wrapper['class'] ?></code>
                                        </p>
                                    </div>
                            </div>
                            <div class="u-uikit__card-body__content-text">
                            </div>
                        </div>
                        <div class="u-uikit__card-body__content-code">
                            <pre><code class="language-markup"><?= htmlentities('<div class="'.$wrapper['class'].'"></div>'); ?></code></pre>
                        </div>
                        <div class="u-uikit__card-body__content-code"><pre><code class="language-css"><?= $wrapper['variable'] ?>: <?= $wrapper['width'] ?></code></pre></div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>

            <div class="card-header u-uikit__card__title">Spacing</div>

            <div class="card u-uikit__card--noborder">
                <div class="card-body u-uikit__card-body">
                    <div>
                        <div class="u-uikit__card-body__content">
                            <div class="u-uikit__card-body__content-wrapper">
                                <div class="u-uikit__card-body__content-info">
                                    <table class="o-table">
                                        <thead>
                                        <tr>
                                            <th>Spacing</th>
                                            <th>Sass variable</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><?= $layouts['spacedefault']/$layouts['spacedefault'];?>px</td>
                                            <td>--space-xxxxs</td>
                                        </tr>
                                        <tr>
                                            <td><?= $layouts['spacedefault']/2;?>px</td>
                                            <td>--space-xxxs</td>
                                        </tr>
                                        <tr>
                                            <td><?= $layouts['spacedefault'];?>px</td>
                                            <td>--space-xxs</td>
                                        </tr>
                                        <tr>
                                            <td><?= $layouts['spacedefault']*2;?>px</td>
                                            <td> --space-xs</td>
                                        </tr>
                                        <tr>
                                            <td><?= $layouts['spacedefault']*4;?>px</td>
                                            <td>--space-sm</td>
                                        </tr>
                                        <tr>
                                            <td><?= $layouts['spacedefault']*6;?>px</td>
                                            <td>--space-md</td>
                                        </tr>
                                        <tr>
                                            <td><?= $layouts['spacedefault']*12;?>px</td>
                                            <td>--space-lg</td>
                                        </tr>
                                        <tr>
                                            <td><?= $layouts['spacedefault']*24;?>px</td>
                                            <td>--space-xl</td>
                                        </tr>
                                        <tr>
                                            <td><?= $layouts['spacedefault']*48;?>px</td>
                                            <td>--space-xxl</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="u-uikit__card-body__content-text">
                                <div class="u-uikit__card-body__content-code">
                                    <pre><code class="language-scss">:root {
    // Spacing
    --space-default: <?= $layouts['spacedefault'];?>px;
    --gap-default: calc(2 * var(--space-default));
    --space-xxxxs: calc(var(--space-default) / 4);
    --space-xxxs: calc(var(--space-default) / 2);
    --space-xxs: calc(var(--space-default));
    --space-xs: calc(2 * var(--space-default));
    --space-sm: calc(4 * var(--space-default));
    --space-md: calc(6 * var(--space-default));
    --space-lg: calc(12 * var(--space-default));
    --space-xl: calc(24 * var(--space-default));
    --space-xxl: calc(48 * var(--space-default));
}

// Usage
@include e(title) {
  margin-top: var(--space-xs);
}
</code></pre>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>

                    </div>
                </div>
            </div>

            <div class="card-header u-uikit__card__title">Padding / Margin</div>

            <div class="card u-uikit__card--noborder">
                <div class="card-body u-uikit__card-body">
                    <div class="u-uikit__copy">
                        <?php
                        foreach ($layouts['spacing'] as $key => $spacing): ?>
                            <p><?= $spacing['name']; ?>: <?= $spacing['value']; ?></p>
                        <?php endforeach; ?>
                    </div>
                    <div>
                        <div class="u-uikit__card-body__content-code">
                                    <pre><code class="language-scss">/* Spacing */
:root {
 <?php
 foreach ($layouts['spacing'] as $key => $spacing): ?>
  <?= $spacing['variable']; ?>: <?= $spacing['value']; ?>
 <?php endforeach; ?>
<br>}

// Usage
@include e(title) {
<?php
foreach ($layouts['spacing'] as $key => $spacing): ?>
  <?= $spacing['attribute']; ?>: var(<?= $spacing['variable']; ?>)
<?php endforeach; ?>
  margin-bottom: space-mult(3);
}
</code></pre>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-header u-uikit__card__title">Breakpoints</div>

            <div class="card u-uikit__card--noborder">
                <div class="card-body u-uikit__card-body">
                    <div class="u-uikit__copy">
                        <p></p>
                    </div>
                    <div>
                        <div class="u-uikit__card-body__content">
                            <div class="u-uikit__card-body__content-wrapper u-uikit__card-body__content-wrapper--full">
                                <div class="u-uikit__card-body__content-info">
                                    <?php
                                    foreach ($layouts['breakpoints'] as $key => $breakpoint): ?>
                                        <div class="u-uikit__card-body__content-block">
                                            <p class="u-uikit__card-body__content-title"><?= $breakpoint['name']; ?></p>
                                            <p class="u-uikit__card-body__content-copy">< <?= $breakpoint['value']; ?></p>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <div>
<pre><code class="language-sass">$breakpoints: (
<?php
foreach ($layouts['breakpoints'] as $key => $breakpoint): ?>
    "<?= $breakpoint['name']; ?>": <?= $breakpoint['value']; ?>,
<?php endforeach; ?>
);

// Usage
// type: max/min default = max
@include mq({breakpoint}, {type}) {
    ...
}
</code></pre>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include(__DIR__ . '/../../template-parts/footer.php'); ?>
