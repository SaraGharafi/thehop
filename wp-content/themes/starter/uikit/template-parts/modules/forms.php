<?php include(__DIR__ . '/../../template-parts/head.php'); ?>
<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="u-wrapper u-wrapper--narrow">
      <div class="u-uikit__header">
        <p class="u-uikit__header__pretitle">Components</p>
        <p class="u-uikit__header__title">Forms</p>
      </div>

        <div class="card-header u-uikit__card__title">Form components</div>

        <div class="card">
            <div class="card-body u-uikit__card-body">
                <div>
                    <div class="u-uikit__card-body__title">Label default</div>
                    <div class="u-uikit__card-body__content">
                        <div class="u-uikit__card-body__content-wrapper">
                            <div>
                                <p class="u-uikit__card-body__content-title">Code</p>
                                <p class="u-uikit__card-body__content-copy"><code class="highlighter-rouge">o-form__label</code></p>
                            </div>
                        </div>
                        <div class="u-uikit__card-body__content-text u-uikit__card-body__content-text--form">
                            <label for="" class="o-form__label">Label</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-body u-uikit__card-body">
                <div>
                    <div class="u-uikit__card-body__title">Text default</div>
                    <div class="u-uikit__card-body__content">
                        <div class="u-uikit__card-body__content-wrapper">
                            <div>
                                <p class="u-uikit__card-body__content-title">Code</p>
                                <p class="u-uikit__card-body__content-copy"><code class="highlighter-rouge">o-form__control</code></p>
                            </div>
                        </div>
                        <div class="u-uikit__card-body__content-text u-uikit__card-body__content-text--form">
                            <form class="o-form">
                                <input type="text" name="" value="" class="o-form__control" placeholder="text">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body u-uikit__card-body">
                <div>
                    <div class="u-uikit__card-body__title">Form select</div>
                    <div class="u-uikit__card-body__content">
                        <div class="u-uikit__card-body__content-wrapper">
                            <div>
                                <p class="u-uikit__card-body__content-title">Code</p>
                                <p class="u-uikit__card-body__content-copy"><code class="highlighter-rouge">o-form__control</code></p>
                            </div>
                        </div>
                        <div class="u-uikit__card-body__content-text u-uikit__card-body__content-text--form">
                            <select name="" class="o-form__control">
                                <option value="">Text 1</option>
                                <option value="">Text 2</option>
                                <option value="">Text 3</option>
                                <option value="">Text 4</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body u-uikit__card-body">
                <div>
                    <div class="u-uikit__card-body__title">Form textarea</div>
                    <div class="u-uikit__card-body__content">
                        <div class="u-uikit__card-body__content-wrapper">
                            <div>
                                <p class="u-uikit__card-body__content-title">Code</p>
                                <p class="u-uikit__card-body__content-copy"><code class="highlighter-rouge">o-form__control</code></p>
                            </div>
                        </div>
                        <div class="u-uikit__card-body__content-text u-uikit__card-body__content-text--form">
                            <textarea class="o-form__control">text</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body u-uikit__card-body">
                <div>
                    <div class="u-uikit__card-body__title">Form radio</div>
                    <div class="u-uikit__card-body__content">
                        <div class="u-uikit__card-body__content-wrapper">
                            <div>
                                <p class="u-uikit__card-body__content-title">Code</p>
                                <p class="u-uikit__card-body__content-copy"><code class="highlighter-rouge">o-form__input-radio</code></p>
                            </div>
                        </div>
                        <div class="u-uikit__card-body__content-text u-uikit__card-body__content-text--input">
                            <input class="o-form__input-radio" type="radio">
                            <input class="o-form__input-radio" type="radio" checked>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body u-uikit__card-body">
                <div>
                    <div class="u-uikit__card-body__title">Form checkbox</div>
                    <div class="u-uikit__card-body__content">
                        <div class="u-uikit__card-body__content-wrapper">
                            <div>
                                <p class="u-uikit__card-body__content-title">Code</p>
                                <p class="u-uikit__card-body__content-copy"><code class="highlighter-rouge">o-form__input-checkbox</code></p>
                            </div>
                        </div>
                        <div class="u-uikit__card-body__content-text u-uikit__card-body__content-text--input">
                            <input class="o-form__input-checkbox" type="checkbox">
                            <input class="o-form__input-checkbox" type="checkbox" checked>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body u-uikit__card-body">
                <div>
                    <div class="u-uikit__card-body__title">Form submit</div>
                    <div class="u-uikit__card-body__content">
                        <div class="u-uikit__card-body__content-wrapper">
                            <div>
                                <p class="u-uikit__card-body__content-title">Code</p>
                                <p class="u-uikit__card-body__content-copy"><code class="highlighter-rouge">o-form__submit</code></p>
                            </div>
                        </div>
                        <div class="u-uikit__card-body__content-text u-uikit__card-body__content-text--form">
                            <input class="o-button o-button--color-primary" type="submit" value="Send">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-header u-uikit__card__title">Forms</div>

        <?php include('parts/forms/default.php'); ?>

    </div>
  </div>
</div>
<?php include(__DIR__ . '/../../template-parts/footer.php'); ?>


