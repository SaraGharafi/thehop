<?php include(__DIR__ . '/../../template-parts/head.php'); ?>

<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="u-wrapper u-wrapper--narrow">
      <div class="u-uikit__header u-wrapper u-wrapper--narrow">
        <p class="u-uikit__header__pretitle">Components</p>
        <p class="u-uikit__header__title">Surface</p>
      </div>

        <?php
           foreach ($surfaces['theme_surfaces'] as $key => $theme_surface): ?>
          <div class="card">
            <div class="card-body u-uikit__card-body">
              <div>
                <div class="u-uikit__card-body__title"><?= $theme_surface['heading']; ?></div>
                <div class="u-uikit__card-body__content">
                  <div class="u-uikit__card-body__content-wrapper">
                    <div>
                      <p class="u-uikit__card-body__content-title">Code</p>
                      <p class="u-uikit__card-body__content-copy"><code class="highlighter-rouge"><?= $theme_surface['code']; ?></code></p>
                    </div>
                  </div>
                  <div class="u-uikit__card-body__content-text u-uikit__card-body__content-text--section">
                    <section class="u-uikit--min-height <?= $theme_surface['code']; ?>"></section>
                  </div>
                </div>
                <div class="u-uikit__card-body__content-code">
                  <pre><code class="language-markup"><?= htmlentities('<section class="' . $theme_surface["code"] . '"></section>'); ?></code></pre>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach; ?>

    </div>
  </div>
</div>
<?php include(__DIR__ . '/../../template-parts/footer.php'); ?>
