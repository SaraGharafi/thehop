<?php include(__DIR__ . '/../../template-parts/head.php'); ?>
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="u-wrapper u-wrapper--narrow">
            <div class="u-uikit__header">
                <p class="u-uikit__header__pretitle">Components</p>
                <p class="u-uikit__header__title">Buttons</p>
            </div>
            <?php
            foreach ($buttons['theme_buttons'] as $key => $theme_button): ?>
            <div class="card">
                <div class="card-body u-uikit__card-body">
                    <div>
                        <div class="u-uikit__card-body__title"><?= $theme_button['heading']; ?></div>
                        <div class="u-uikit__card-body__content">
                            <div class="u-uikit__card-body__content-wrapper">
                                <div>
                                    <p class="u-uikit__card-body__content-title">Code</p>
                                    <p class="u-uikit__card-body__content-copy"><code class="highlighter-rouge"><?= $theme_button['code']; ?></code></p>
                                </div>
                            </div>
                            <div class="u-uikit__card-body__content-text">
                                <p class="u-uikit__card-body__content-copy"><a href="#" class="<?= $theme_button['code']; ?>">button</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php include(__DIR__ . '/../../template-parts/footer.php'); ?>
