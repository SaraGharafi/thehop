<?php
include(__DIR__ . '../../config/theme.php');
$module = ''; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="author" content="Jorge Ruiz - Pixel Division">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?= BASE_URL ?>/assets/img/favicon.png">
    <title>Pixel Division UIKIT</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">

    <!-- Main styles for this application-->
    <link href="<?= BASE_URL ?>/assets/css/admin.min.css" rel="stylesheet">
    <link href="<?= BASE_URL ?>/assets/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
    <link href="<?= BASE_URL ?>/assets/vendors/prism/prism.css" rel="stylesheet"/>

    <!-- Main styles Website -->
    <link href="<?= $path_theme_css; ?>" rel="stylesheet">

    <!-- Main styles for this application-->
    <link href="<?= BASE_URL ?>/assets/css/style.css?date=<?= date('Ymdhis') ?>" rel="stylesheet">

</head>
<body class="app sidebar-fixed aside-menu-fixed sidebar-lg-show">
<?php
include(__DIR__ . '/header.php');
include(__DIR__ . '/menu.php'); ?>
<main class="main">
