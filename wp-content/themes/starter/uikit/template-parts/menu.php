<div class="app-body">
    <div class="sidebar">
        <nav class="sidebar-nav">
            <ul class="nav">
                <li class="nav-item nav-item-brand">
                    <a class="nav-link nav-link-brand" href="<?= BASE_URL ?>"><img class="navbar-brand-full" src="<?= BASE_URL ?>/assets/img/logo.svg" alt="Pixel Division Logo"></a>
                </li>

                <?php
                echo 'cuantos';
                echo count($menu);
                if(count($menu) > 0):
                foreach ($menu as $key => $value): ?>
                    <li class="nav-item nav-dropdown open">
                        <a class="nav-link nav-dropdown-toggle u-uikit__menu__title" href="#"><?= $key ?></a>
                        <ul class="nav-dropdown-items">
                            <?php foreach ($value as $menu_key => $menu): ?>
                                <li class="nav-item"><a
                                        class="nav-link u-uikit__menu__link <?php if ($module == $menu_key): ?>u-uikit__menu__link--active<?php endif; ?>"
                                        href="<?= BASE_URL ?>/template-parts/modules/<?= $menu_key ?>.php"><?= $menu ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </li>

                    <li class="nav-divider"></li>
                <?php endforeach;
                endif; ?>

            </ul>
        </nav>
    </div>
