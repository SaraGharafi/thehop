<header class="app-header navbar">
  <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="<?= BASE_URL ?>">
      <img class="navbar-brand-full" src="<?= BASE_URL ?>/assets/img/logo.svg" width="89" height="25" alt="Pixel Division Logo">
  </a>
  </button>
</header>
