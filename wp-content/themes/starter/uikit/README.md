# Pixel Division UI KIT

## Instrucciones

Para empezar ha utilizar el UIKIT hay que bajarse el repositiorio de Bitbucket. Si vas a empezar con un proyecto SAGE ya está incluído en el repositiorio, en la carpeta 'uikit'. Para bajar sólo el uikit usa el siguiente comando.

```
git clone git@bitbucket.org:pixeldivisiondev/base-ui-kit.git .
```

### Instalación

Dentro de la carpeta <i><b>config</b></i> encontraremos los ficheros config que tenemos que modificar. 


#### Theme config
En el fichero <i><b>theme.php</b></i> tenemos que modificar la configuración general del UIKIT. Por ejemplo en la variable $path_theme_css tenemos que poner la ruta de nuestro main.css de la web.

```

/*
 * CSS main de la web
 */
$path_theme_css = BASE_WEB . '/dist/styles/main.css';

```

#### Menu

En el fichero <i><b>menu.php</b></i> vamos creando el menú de la izquierda mediante arrays. La key del array es el nombre del fichero y el valor es el nombre del menú.

```

$menu['Overview'] = array(
    'introduction' => 'Introduction',
);

$menu['Style'] = array(
    'colors' => 'Colors',
    'typography' => 'Typography',
    'layout' => 'Layout',
    'icons' => 'Icons',
);

```

##### Layout

En el fichero <i><b>layout.php</b></i> hay que ir rellenando los arrays con la configuración de grid, wrappers y media queries de nuestra web.

```

$layouts = array(
    'grid' => array(
        array('name' => 'Layout desktop', 'width' => '1440px'),
        array('name' => 'Layout desktop', 'width' => '1110px'),
    ),
    'wrappers' => array(
        array('name' => 'Wrapper', 'class' => 'u_wrapper', 'width' => '1440px', 'variable' => '--wrapper-size'),
        array('name' => 'Wrapper narrow', 'class' => 'u_wrapper u_wrapper_narrow', 'width' => '1110px', 'variable' => '--wrapper-size-narrow'),
    ),
    'spacedefault' => 8,
    'spacing' => array(
        array('name' => 'padding-default', 'attribute' => 'padding', 'value' => '48px', 'variable' => '--padding-default'),
    ),
    'breakpoints' => array(
        array('name' => 'desktop-wide',  'value' => '1440px'),
        array('name' => 'desktop',  'value' => '1248px'),
        array('name' => 'tablet-wide',  'value' => '1024px'),
        array('name' => 'tablet-small',  'value' => '768px'),
        array('name' => 'phablet',  'value' => '640px'),
        array('name' => 'mobile-wide',  'value' => '480px'),
        array('name' => 'mobile',  'value' => '400px'),
    ),
);

```

##### Colors

En el fichero <i><b>colors.php</b></i> hay que ir rellenando los arrays según nuestas variables creadas en colors de nuestro sass de la web.

```

$colors = array(
    'theme' => array(
        '--color-primary',
        '--color-secondary'
    ),
    'grayscale' => array(
        '--color-white',
        '--color-gray-100',
        '--color-gray-200',
        '--color-gray-300',
        '--color-gray-400',
        '--color-gray-500',
        '--color-gray-600',
        '--color-gray-700',
        '--color-gray-800',
        '--color-gray-900',
        '--color-black'
    ),
    'additional' => array(
        '--color-success',
        '--color-error',
        '--color-warning',
        '--color-info'
    )
);

```

##### Typography

En el fichero <i><b>fonts.php</b></i> hay que ir rellenando los arrays según nuestas variables creadas en fonts de nuestro sass de la web. 

```

$fonts = array(
    'theme_fonts' => array(
        array('type' => 'primary', 'name' => 'Open Sans', 'weight' => 'bold'),
        array('type' => 'secondary', 'name' => 'Open Sans', 'weight' => 'regular'),
        array('type' => 'tertiary', 'name' => 'Open Sans', 'weight' => 'regular'),
    ),
    'font_usage' => array(
        array('type' => 'primary', 'name' => 'Open Sans', 'weight' => 'bold'),
        array('type' => 'secondary', 'name' => 'Open Sans', 'weight' => 'regular'),
        array('type' => 'secondary', 'name' => 'Open Sans', 'weight' => 'medium'),
        array('type' => 'secondary', 'name' => 'Open Sans', 'weight' => 'bold'),
        array('type' => 'tertiary', 'name' => 'Open Sans', 'weight' => 'regular'),
        array('type' => 'tertiary', 'name' => 'Open Sans', 'weight' => 'bold'),
    ),
    'text' => array(
        array('heading' => 'Text', 'code' => 'u-text', 'type' => 'dark'),
    ),
    'paragraphs' => array(
        array('heading' => 'Paragraph', 'code' => 'u-text', 'type' => 'dark'),
    ),
    'links' => array(
        array('heading' => 'Link', 'code' => 'u-text', 'type' => 'dark'),
    ),
    'lists' => array(
        array('heading' => 'List', 'code' => 'u-text', 'type' => 'dark'),
    ),
);

```

##### Butttons

En el fichero <i><b>buttons.php</b></i> hay que ir rellenando los arrays según nuestas variables creadas en buttons de nuestro sass de la web. 

```

$buttons = array(
    'theme_buttons' => array(
        array('heading' => 'Default', 'code' => 'o-button'),
        array('heading' => 'Button primary', 'code' => 'o-button o-button--color-primary'),
        array('heading' => 'Button secondary', 'code' => 'o-button o-button--color-secondary'),
        array('heading' => 'Button Outline', 'code' => 'o-button o-button--outline'),
        array('heading' => 'Button Size small', 'code' => 'o-button o-button--color-primary o-button--size-sm'),
        array('heading' => 'Button Size Medium', 'code' => 'o-button o-button--color-primary o-button--size-md'),
        array('heading' => 'Button Size Big', 'code' => 'o-button o-button--color-primary o-button--size-lg'),
        array('heading' => 'Button Size Full', 'code' => 'o-button o-button--color-primary o-button--size-full'),
    ),
);

```

##### Icons

En el fichero <i><b>icons.php</b></i> hay que ir rellenando los arrays con los logos y modificar la URL de nuestro scss con los iconos. 

```

$path_theme_icons = SAGE_PATH . '/resources/assets/styles/utilities/_utilities.icons.scss';

$icons = array(
    'logos' => array(
        array('name' => 'default', 'url' => BASE_WEB .'/resources/assets/images/logo.svg', 'type' => 'dark'),
        array('name' => 'dark', 'url' => BASE_URL .'/assets/img/brand/logo-habitant-dark.jpg', 'type' => 'light'),
        array('name' => 'light', 'url' => BASE_URL .'/assets/img/brand/logo-habitant-light.jpg', 'type' => 'light'),
    ),
);

```

##### Surface

En el fichero <i><b>surface.php</b></i>  hay que ir rellenando los arrays según nuestas variables creadas en surface de nuestro sass de la web.

```

$surfaces = array(
    'theme_surfaces' => array(
        array('heading' => 'Color white', 'code' => 'u-surface u-surface-–color-white'),
        array('heading' => 'Color primary', 'code' => 'u-surface u-surface--color-primary'),
        array('heading' => 'Color secondary', 'code' => 'u-surface u-surface--color-secondary'),
        array('heading' => 'Color Gray 100', 'code' => 'u-surface u-surface--color-gray-100'),
    ),
);


```

#### Footer

En el fichero <i><b>template-parts/footer.php</b></i> vamos añadiendo según el modulo (key del array del menu) ficheros js..

```

<?php if ($module == 'colors'): ?>
    <script src="assets/js/colors.js"></script>
<?php endif; ?>

<?php if ($module == 'typography'): ?>
    <script src="assets/js/typography.js"></script>
<?php endif; ?>
);

```

#### Módulos

Dentro de template-parts/modules/ vamos ha encontrar los distintos módulos de nuestro UIKIT. Si queremos crear uno nuevo, duplicamos el fichero <i><b>default.php</b></i> y vamos rellenando con el componente que vamos a crear.

```

<div class="u-uikit__card-body__content-text">

<!-- Aqui va el código -->

</div>

```

## Version

3.0 BETA

## Authors

* **Jorge Ruiz** - *Initial work*
