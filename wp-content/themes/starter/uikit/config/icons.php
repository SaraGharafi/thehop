<?php

$path_theme_icons = SAGE_PATH . '/resources/assets/styles/utilities/_utilities.icons.scss';

$icons = array(
    'logos' => array(
        array('name' => 'default', 'url' => BASE_WEB .'/resources/assets/images/logo.svg', 'type' => 'dark'),
        array('name' => 'dark', 'url' => BASE_URL .'/assets/img/brand/logo-habitant-dark.jpg', 'type' => 'light'),
        array('name' => 'light', 'url' => BASE_URL .'/assets/img/brand/logo-habitant-light.jpg', 'type' => 'light'),
    ),
);

?>
