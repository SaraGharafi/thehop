<?php

$colors = array(
    'theme' => array(
        '--color-primary',
        '--color-secondary'
    ),
    'secondary' => array(
        "--color-white",
        "--color-black",
    ),
    'grayscale' => array(
        '--color-white',
        '--color-gray-100',
        '--color-gray-200',
        '--color-gray-300',
        '--color-gray-400',
        '--color-gray-500',
        '--color-gray-600',
        '--color-gray-700',
        '--color-gray-800',
        '--color-gray-900',
        '--color-black'
    ),
    'additional' => array(
        '--color-success',
        '--color-error',
        '--color-warning',
        '--color-info'
    )
);

?>
