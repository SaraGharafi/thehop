<?php

$fonts = array(
    'theme_fonts' => array(
        array('type' => 'primary', 'name' => 'Open Sans', 'weight' => 'bold'),
        array('type' => 'secondary', 'name' => 'Open Sans', 'weight' => 'regular'),
        array('type' => 'tertiary', 'name' => 'Open Sans', 'weight' => 'regular'),
    ),
    'font_usage' => array(
        array('type' => 'primary', 'name' => 'Open Sans', 'weight' => 'bold'),
        array('type' => 'secondary', 'name' => 'Open Sans', 'weight' => 'regular'),
        array('type' => 'secondary', 'name' => 'Open Sans', 'weight' => 'medium'),
        array('type' => 'secondary', 'name' => 'Open Sans', 'weight' => 'bold'),
        array('type' => 'tertiary', 'name' => 'Open Sans', 'weight' => 'regular'),
        array('type' => 'tertiary', 'name' => 'Open Sans', 'weight' => 'bold'),
    ),
    'text' => array(
        array('heading' => 'Text', 'code' => 'u-text', 'type' => 'dark'),
    ),
    'paragraphs' => array(
        array('heading' => 'Paragraph', 'code' => 'u-text', 'type' => 'dark'),
    ),
    'links' => array(
        array('heading' => 'Link', 'code' => 'u-text', 'type' => 'dark'),
    ),
    'lists' => array(
        array('heading' => 'List', 'code' => 'u-text', 'type' => 'dark'),
    ),
);

?>
