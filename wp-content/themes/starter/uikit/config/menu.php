<?php

$menu = array();
$menu['Overview'] = array(
    'introduction' => 'Introduction',
);

$menu['Style'] = array(
    'colors' => 'Colors',
    'typography' => 'Typography',
    'layout' => 'Layout',
    'icons' => 'Icons',
);

$menu['Components'] = array(
    'surface' => 'Surface',
    'buttons' => 'Buttons',
    'forms' => 'Forms',
);

$menu['Optionals'] = array(
    'cards' => 'Cards',
);

$menu['Navigation'] = array(
    'breadcrumbs' => 'Breadcrumbs',
    'pagination' => 'Pagination',
);

?>
