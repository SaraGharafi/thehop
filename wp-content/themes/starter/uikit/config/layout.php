<?php

$layouts = array(
    'grid' => array(
        array('name' => 'Layout desktop', 'width' => '1440px'),
        array('name' => 'Layout desktop', 'width' => '1110px'),
    ),
    'wrappers' => array(
        array('name' => 'Wrapper', 'class' => 'u_wrapper', 'width' => '1440px', 'variable' => '--wrapper-size'),
        array('name' => 'Wrapper narrow', 'class' => 'u_wrapper u_wrapper_narrow', 'width' => '1110px', 'variable' => '--wrapper-size-narrow'),
    ),
    'spacedefault' => 8,
    'spacing' => array(
        array('name' => 'padding-default', 'attribute' => 'padding', 'value' => '48px', 'variable' => '--padding-default'),
    ),
    'breakpoints' => array(
        array('name' => 'desktop-wide',  'value' => '1440px'),
        array('name' => 'desktop',  'value' => '1248px'),
        array('name' => 'tablet-wide',  'value' => '1024px'),
        array('name' => 'tablet-small',  'value' => '768px'),
        array('name' => 'phablet',  'value' => '640px'),
        array('name' => 'mobile-wide',  'value' => '480px'),
        array('name' => 'mobile',  'value' => '400px'),
    ),
);

?>
